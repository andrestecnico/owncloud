FROM ubuntu:18.04

ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install curl gnupg -y
RUN  apt-get install -y \
    apache2 \
    libapache2-mod-php \
    php-gd \
    php-json \
    php-mysql \
    php-sqlite3 \
    php-curl \
    php-intl \
    php-imagick \
    php-zip \
    php-xml \
    php-mbstring \
    php-soap \
    php-ldap \
    php-apcu \
    php-redis \
    php-dev \
    libsmbclient-dev \
    php-gmp \
    smbclient 

ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_PID_FILE /var/run/apache2/apache2.pid
ENV APACHE_RUN_DIR /var/run/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_LOG_DIR /var/log/apache2
RUN mkdir -p $APACHE_RUN_DIR
RUN mkdir -p $APACHE_LOCK_DIR
RUN mkdir -p $APACHE_LOG_DIR
EXPOSE 80
#owc
RUN echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/server:/10/Ubuntu_18.04/ /' | tee /etc/apt/sources.list.d/isv:ownCloud:server:10.list \
    && curl -fsSL https://download.opensuse.org/repositories/isv:ownCloud:server:10/Ubuntu_18.04/Release.key | gpg --dearmor |  tee /etc/apt/trusted.gpg.d/isv_ownCloud_server_10.gpg > /dev/null \
    && apt-get update && apt-get install owncloud-complete-files -y

RUN chown -R www-data: /var/www/owncloud
RUN echo "ServerName localhost" >> /etc/apache2/apache2.conf
COPY owncloud.conf /etc/apache2/sites-available/ 
RUN ln -s /etc/apache2/sites-available/owncloud.conf /etc/apache2/sites-enabled/owncloud.conf

ENTRYPOINT ["/usr/sbin/apache2"]
CMD ["-D", "FOREGROUND"]
